package exnihilo.registries.helpers;

public class HeatSource {
	public int id;
	public int meta;
	public float value;
	
	public HeatSource(int id, int meta, float value)
	{
		this.id = id;
		this.meta = meta;
		this.value = value;
	}
}
